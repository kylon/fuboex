# fuboEx

Requirements:

 - [xar](http://mackyle.github.io/xar/)  
 - [Lekensteyn pbzx script](https://gist.github.com/Lekensteyn/6e0840e77bc9bd013f57)  
 - [UEFIExtract](https://github.com/LongSoft/UEFITool)  
 - 9GB of RAM (yeah, maybe it can be optimized)  

## Description
The output is a php file: _fuboExData.php_.  

It only supports Firmware and BridgeOS pkgs.

It will extract OEM strings and bios version.

## Usages
fuboEx.py _filename_  (get data from _filename_)
 
fuboEx.py _foldername_  (get data from all the files in _foldername_) (_foldername_ can contain both Firmware and BridgeOS pkgs)
 
## Known issues
It does not remove duplicates when the model is the same but firmware version is older.

Do not mix old and new pkgs or open a PR :P
