#!/usr/bin/python

'''
	FirmwareUpdate BridgeOSUpdate Extractor @kylon - 2019

	Credits:
		vit9696 - a lot of help
		Sherlocks - same as above :)
		pudquick - pbzx original script
		Lekensteyn - updated version of pudquick's script
'''

import os
import re
import sys
import shutil
import chardet
import itertools
import subprocess
from pathlib import Path
from abc import ABC, abstractmethod
from distutils.spawn import find_executable

class PKGExtractor(ABC):
	__GUIDList = [
		"C3E36D09-8294-4B97-A857-D5288FE33E28",
		"B535ABF6-967D-43F2-B494-A1EB8E21A28E"
	]
	__outFoldersList = [
		"legacyBios",
		"oemStrings"
	]
	__uefiExtract = ""
	__pkgPath = ""

	def __init__(self, pkgPath):
		self.__uefiExtract = Path("UEFIExtract").resolve()
		self.__pkgPath = Path(pkgPath).resolve()

	def getGUIDList(self):
		return self.__GUIDList

	def getOutFoldersList(self):
		return self.__outFoldersList

	def getUEFIExtract(self):
		return self.__uefiExtract

	def getPkg(self):
		return self.__pkgPath

	def extractData(self, path):
		args = [self.getUEFIExtract(), path]
		mode = []

		for folder in self.getOutFoldersList():
			mode.append("file")

		args.extend(self.getGUIDList())
		args.append("-o")
		args.extend(self.getOutFoldersList())
		args.append("-m")
		args.extend(mode)

		subprocess.run(args)

	def parseLegacyBios(self):
		ret = []

		ffsPath = Path(self.getOutFoldersList()[0]).glob('*.ffs')
		for ffs in ffsPath:
			fd = open(ffs, "rb")

			for line in fd: # probably bad
				matches = re.findall("I\$\ ([\d\w\s\\\.]+)C", str(line))
				legacyVer = []

				if len(matches) == 0:
					continue

				matchSplit = matches[0].split('.')
				for byteStr in matchSplit:
					legacyVer.append(''.join(re.findall("\\\\x0{2}([\w\d]{1})", byteStr)))

					matches = '.'.join(legacyVer)

				ret.append(matches)

		ret.sort()

		noDup = list(ret for ret,_ in itertools.groupby(ret))

		return noDup

	def parseOEMStrings(self):
		ret = []

		ffsPath = Path(self.getOutFoldersList()[1]).glob('*.ffs')
		for ffs in ffsPath:
			fd = open(ffs, "rb")
			tmp = []

			for line in fd: # probably bad
				if chardet.detect(line)['encoding'] != 'ascii' or line[0] == 0:
					continue

				matches = re.findall("^b'  ([a-zA-Z\s]+:)\ +([\w\d\s.:()-@&]+)", str(line))
				fstr = ""

				for match in matches:
					for value in match:
						fstr += value

						if fstr[-1:] == ':':
							fstr += ' '

				tmp.append(fstr)

			ret.append(tmp)

		ret.sort()

		noDup = list(ret for ret,_ in itertools.groupby(ret))

		return noDup

	@abstractmethod
	def clean(self):
		pass

	@abstractmethod
	def unpackPkg(self):
		pass

	@abstractmethod
	def unzipPkg(self):
		pass

	@abstractmethod
	def extractUEFIData(self):
		pass

	@abstractmethod
	def genOutput(self):
		pass

class FirmwareUpdate(PKGExtractor):
#	__cecekpawonScript = ""

#	def __init__(self):
#		self.__cecekpawonScript = (cwd / "yod-NakedAppleFirmwareUpdatePkg.php").resolve()

	def clean(self):
		os.remove("PackageInfo")
		os.remove("Scripts.zip")
		os.remove("Scripts")
#		os.remove("ceceOut.txt")

		if Path("Bom").exists():
			os.remove("Bom")

		if Path("Payload").exists():
			os.remove("Payload")

		for folder in self.getOutFoldersList():
			shutil.rmtree(folder + "/")

	def unpackPkg(self):
		subprocess.run(["xar", "-xf", self.getPkg()])
		os.rename("Scripts", "Scripts.zip")

	def unzipPkg(self):
		subprocess.run(["7z", "e", "Scripts.zip"])

	def extractUEFIData(self):
		self.extractData("Scripts")

	'''
	def runCecekpawonScript(self, smcArr): # temp?
		out = open("ceceOut.txt", "w")
		start = False

		subprocess.run(["php", self.__cecekpawonScript], stdout=out)
		out.close()

		out = open("ceceOut.txt", "r")
		for line in out:
			if not start:
				start = True if "SMCJSONs:" in line else False
				continue

			matches = re.findall("- ([\w]+-[\w\d]+):\s?([\d]+.[\w\d]+)", line)
			fstr = ""

			if len(matches) == 0:
				continue

			for match in matches:
				space = ' ' if fstr == "" else ''

				for val in match:
					fstr += val + space

			smcArr.append(fstr)
	'''

	def genOutput(self):
		smcVers = []

		self.unpackPkg()
		self.unzipPkg()
		self.extractUEFIData()
#		self.runCecekpawonScript(smcVers)

		legacyBios = self.parseLegacyBios()
		oemStrings = self.parseOEMStrings()

		self.clean()

		outLeg = open("legacyBios.txt", "a+")

		for legacyV in legacyBios:
			model = legacyV.split('.')[0]

			outLeg.write("\t\'"+model+"\' => \'"+legacyV+"\',\n")

		outLeg.close()

		outOem = open("oemstrings.txt", "a+")

		for oemS in oemStrings:
			hasModel = False
			model = ""
			fstr = ""

			for val in oemS:
				if val[0] == 'M':
					hasModel = True
					model = val.split(':')[1].strip(' ')
					break

				elif "BIOS " in val:
					hasModel = True
					model = val.split(':')[1].split('.')[0].strip(' ')
					break

			if not hasModel:
				print("Cannot find model:\n"+val+"\n\n")
				continue

			for vv in oemS:
				tmp = vv.split(': ')
				tmpLen = len(tmp[0])

				fstr += "\t"+tmp[0]+':'

				while tmpLen < 12:
					fstr += ' '
					tmpLen += 1

				fstr += tmp[1]+"\n"

			fstr = fstr[:-1]
			outOem.write("\t\'"+model+"\' => \'\n  Apple ROM Version\n"+fstr+"\',\n\n")

		outOem.close()

class BridgeOSUpdate(PKGExtractor):
	__tmpFold = 'efiout'

	def getTmpFolder(self):
		return self.__tmpFold

	def clean(self):
		os.remove('PackageInfo')
		os.remove('Bom')
		os.remove('Payload')
		os.remove('payloadUnc')

		shutil.rmtree("usr/")
		shutil.rmtree("bundleUp/")
		shutil.rmtree(self.getTmpFolder()+'/')

		for folder in self.getOutFoldersList():
			shutil.rmtree(folder + "/")

	def unpackPkg(self):
		subprocess.run(["xar", "-xf", self.getPkg()])

	def unzipPkg(self):
		bundlePath = Path('usr/standalone/firmware/bridgeOSCustomer.bundle/Contents/Resources/UpdateBundle.zip').resolve()

		subprocess.run(["unzip", "-o", bundlePath, "-d", "bundleUp"])

	def unpackPayload(self):
		payload = subprocess.Popen(("cat", "Payload"), stdout=subprocess.PIPE)
		content = subprocess.check_output(("./parse_pbzx.py"), stdin=payload.stdout, shell=True)
		payload.wait()

		fd = open("payloadUnc", "wb")
		fd.write(content)
		fd.close()

		bridgePbzx = subprocess.Popen(("cat", "payloadUnc"), stdout=subprocess.PIPE)
		subprocess.check_output(("cpio -id"), stdin=bridgePbzx.stdout, shell=True)
		bridgePbzx.wait()

	def extractUEFIData(self):
		extractedPath = Path('bundleUp/boot/Firmware/MacEFI/').resolve().glob('**/*.im4p')
		i = 0
		j = 0

		os.mkdir(self.getTmpFolder())

		for fold in self.getOutFoldersList():
			os.mkdir(fold)

		os.chdir(self.getTmpFolder())

		for file in extractedPath:
			fold = 'b'+str(i)
			fwPath = Path(fold+'/efi').resolve()

			os.mkdir(fold)
			os.chdir(fold)
			os.rename(str(file), fwPath)
			self.extractData(fwPath)

			for extFold in self.getOutFoldersList():
				epath = Path(extFold).resolve().glob('**/*.ffs')

				for efile in epath:
					os.rename(str(efile), Path('../../'+extFold+'/ext'+str(j)+'.ffs').resolve())

					j += 1

			os.chdir('..')
			i += 1

		os.chdir('..')

	def genOutput(self):
		self.unpackPkg()
		self.unpackPayload()
		self.unzipPkg()
		self.extractUEFIData()

		legacyBios = self.parseLegacyBios()
		oemStrings = self.parseOEMStrings()

		self.clean()

		outLeg = open("legacyBios.txt", "a+")

		for legacyV in legacyBios:
			model = legacyV.split('.')[0]

			outLeg.write("\t\'"+model+"\' => \'"+legacyV+"\',\n")

		outLeg.close()

		outOem = open("oemstrings.txt", "a+")

		for oemS in oemStrings:
			hasModel = False
			model = ""
			fstr = ""

			for val in oemS:
				if val[0] == 'M':
					hasModel = True
					model = val.split(':')[1].strip(' ')
					break

				elif "BIOS " in val:
					hasModel = True
					model = val.split(':')[1].split('.')[0].strip(' ')
					break

			if not hasModel:
				print("Cannot find model:\n"+val+"\n\n")
				continue

			for vv in oemS:
				tmp = vv.split(':')
				tmpLen = len(tmp[0])

				fstr += "\t"+tmp[0]+':'

				while tmpLen < 12:
					fstr += ' '
					tmpLen += 1

				fstr += tmp[1]+"\n"

			fstr = fstr[:-1]
			outOem.write("\t\'"+model+"\' => \'\n  Apple ROM Version\n"+fstr+"\',\n\n")

		outOem.close()

def printHead():
	ver = "1.0.1"

	print("FirmwareUpdateBridgeOSUpdateExtractor (fuboEx)\nv"+ver+" @kylon\n")

def isInvalidEnv():
	return bool(find_executable("xar") is None) or bool(find_executable("7z") is None)

def envError():
	print("fuboEx requires xar and 7z to work.\nPlease install them.")
	quit()

def fileError():
	print("Unknown file name.\n\nSupported: BridgeOS*, FirmwareUpdate*\n")
	quit()

def usage():
	print("usage:\n\n")
	print("fuboEx.py [fileName] (get data from fileName)\n")
	print("fuboEx.py [folderName] (get data from all the files in folderName)\n\n")
	quit()

def run(fileName):
	obj = None

	if "BridgeOS" in fileName:
		obj = BridgeOSUpdate(fileName)

	elif "FirmwareUpdate" in fileName:
		obj = FirmwareUpdate(fileName)

	else:
		return False

	obj.genOutput()
	return True

def main():
	argc = len(sys.argv)

	printHead()

	if isInvalidEnv() is True:
		envError()

	elif argc < 2:
		usage()

	arg1 = Path(sys.argv[1])

	if arg1.is_file() is True:
		ret = run(str(arg1))

		if ret is False:
			fileError()

	elif arg1.is_dir() is True:
		arg1 = arg1.resolve()
		tmp = str(arg1).split('/')

		tmp.pop(-1)

		arg1 = Path('/'.join(tmp)).resolve()

		pkgsPath = arg1.glob('**/*.pkg')
		for file in pkgsPath:
			ret = run(str(file))

			if ret is False:
				fileError()

	else:
		print("Unknown input\n")
		return

	# gen php
	outPHP = open("fuboExData.php", "w")
	tmp = []

	outPHP.write("<?php\n$legacyBios = [\n")

	with open("legacyBios.txt", "r") as lg:
		for line in lg:
			tmp.append(line)

	tmp.sort()

	noDup = list(tmp for tmp,_ in itertools.groupby(tmp))
	for item in noDup:
		outPHP.write(item)

	outPHP.write("];\n\n$oemStrings = [\n")
	tmp.clear()

	with open("oemstrings.txt", "r") as oem:
		tic = 0
		tmp1 = []

		for line in oem:
			if "=>" in line and tic != 0:
				itm = tmp1.copy()
				tmp.append(itm)
				tmp1.clear()

			tmp1.append(line)
			tic = 1

	tmp.append(tmp1) # last one
	tmp.sort()

	noDup = list(tmp for tmp,_ in itertools.groupby(tmp))
	for item in noDup:
		for line in item:
			outPHP.write(line)

	outPHP.write("];\n")
	outPHP.close()

	os.remove('legacyBios.txt')
	os.remove('oemstrings.txt')

# run
main()
